/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include <gtk/gtk.h>
#include "helloworld-cpp-library.h"

int
main (int argc, char **argv)
{
  /* FIXME: We want to use GApplication.
   * https://phabricator.apertis.org/T3212 */

  gtk_init(&argc, &argv);

  HelloWorldCppLibrary example = HelloWorldCppLibrary ();

  gtk_main ();

  return 0;
}
