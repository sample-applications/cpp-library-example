/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include <glib-object.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "helloworld-cpp-library.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic pop

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#pragma GCC diagnostic ignored "-Wdeprecated-copy"
#pragma GCC diagnostic ignored "-Wcast-align"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#pragma GCC diagnostic pop

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

using cv::Mat;
using std::vector;

static GtkWidget *
get_widget_from_pixbuf (GdkPixbuf *pixbuf)
{
  GtkWidget *image = gtk_image_new_from_pixbuf (pixbuf);

  gtk_widget_set_size_request (image,
                          gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));
 
  return image;
}
static void
destroy_image_data (guchar *data, gpointer userdata)
{
  delete data;
}

static GtkWidget *
get_widget_from_opencv_color_image (const Mat& image)
{
  /* FIXME: We can get rid of using GdkPixbuf.
   * https://phabricator.apertis.org/T3211 */
  size_t bytes = image.total() * image.elemSize();
  guchar *data = new guchar[bytes];
  memcpy(data, image.data, bytes);
  g_autoptr(GdkPixbuf) pixbuf = gdk_pixbuf_new_from_data (
              data,
              GDK_COLORSPACE_RGB,
              false,
              8,
              image.cols,
              image.rows,
              image.step,
              destroy_image_data,
              NULL);

  return get_widget_from_pixbuf (pixbuf);
}

static gboolean
toggle_filter_cb (GtkWidget *widget,
                  GdkEventKey *event,
                  gpointer user_data)
{
  GtkStack *stack = GTK_STACK(widget);

  if (g_str_equal(gtk_stack_get_visible_child_name(stack), "plain")) {
    gtk_stack_set_visible_child_name(stack, "filtered");
  } 
  else {
    gtk_stack_set_visible_child_name(stack, "plain");
  }

  return TRUE;
}

HelloWorldCppLibrary::HelloWorldCppLibrary ()
{
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_window_set_default_size(GTK_WINDOW(window), WIN_WIDTH, WIN_HEIGHT);

  create_cat_widgets();

  g_signal_connect (G_OBJECT(window), "destroy", gtk_main_quit, NULL);

  gtk_widget_show_all (window);

}

HelloWorldCppLibrary::~HelloWorldCppLibrary ()
{
}

void HelloWorldCppLibrary::create_cat_widgets ()
{
  /* Init cats */
  GError *err = NULL;

  /* FIXME: We can get rid of using GdkPixbuf.
   * https://phabricator.apertis.org/T3211 */
  GdkPixbuf *cat_pixbuf = gdk_pixbuf_new_from_resource (
    "/org/apertis/HelloWorld/CppLibrary/cat.jpg", &err);

  if (err)
  {
    g_error ("Unable to load image: %s", err->message);
    g_clear_error (&err);
    return;
  }

  Mat opencv_image (
    cv::Size (gdk_pixbuf_get_width (cat_pixbuf),
              gdk_pixbuf_get_height (cat_pixbuf)),
    CV_8UC3,
    gdk_pixbuf_get_pixels (cat_pixbuf),
    gdk_pixbuf_get_rowstride (cat_pixbuf));

  if (opencv_image.empty ()) {
    g_error ("Error converting to OpenCV image.");
    return;
  }

  Mat filtered;
  Canny (opencv_image, filtered, 50, 200, 3);
  cvtColor (filtered, filtered, CV_GRAY2RGB);

  GtkWidget *cat_widget, *cat_filtered_widget;
  
  cat_filtered_widget = get_widget_from_opencv_color_image (filtered);
  cat_widget = get_widget_from_pixbuf (cat_pixbuf);

  g_object_unref(cat_pixbuf);

  GtkWidget *stack = gtk_stack_new();

  gtk_stack_add_named (GTK_STACK(stack), cat_widget, "plain");
  gtk_stack_add_named (GTK_STACK(stack), cat_filtered_widget, "filtered");

  gtk_container_add (GTK_CONTAINER(window), stack);
  gtk_widget_add_events(stack, GDK_BUTTON_PRESS_MASK);

  g_signal_connect (stack, "button-press-event",
    G_CALLBACK (toggle_filter_cb), NULL);
}
