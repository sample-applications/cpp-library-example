/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SRC_HELLOWORLDCPPLIBRARY_H__
#define SRC_HELLOWORLDCPPLIBRARY_H__

#include <gtk/gtk.h>

class HelloWorldCppLibrary
{
  GtkWidget *window;
 public:
  HelloWorldCppLibrary ();
  ~HelloWorldCppLibrary ();
  void create_cat_widgets ();
};

#endif  // SRC_HELLOWORLDCPPLIBRARY_H__
